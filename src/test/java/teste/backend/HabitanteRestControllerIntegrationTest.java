package teste.backend;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import teste.backend.controllers.HabitanteController;
import teste.backend.entities.Habitante;
import teste.backend.services.HabitanteService;


@RunWith(SpringRunner.class)
@WebMvcTest(HabitanteController.class)
public class HabitanteRestControllerIntegrationTest {
 
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private HabitanteService service;
 
    @Test
    public void givenHabitantes_whenGetHabitantes_thenReturnJsonArray()
      throws Exception {
         
    	Habitante alex = new Habitante("1234","alex");
     
        List<Habitante> allHabitantes = Arrays.asList(alex);
     
        given(service.getHabitantes()).willReturn(allHabitantes);
     
        mvc.perform(get("/habitantes")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$", hasSize(1)))
          .andExpect(jsonPath("$[0].codigo", is(alex.getCodigo())));
    }
}
