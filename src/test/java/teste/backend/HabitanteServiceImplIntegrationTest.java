package teste.backend;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import teste.backend.entities.Habitante;
import teste.backend.repository.EnderecoRepository;
import teste.backend.repository.HabitanteRepository;
import teste.backend.services.EnderecoService;
import teste.backend.services.HabitanteService;
import teste.backend.services.ViacepService;

@RunWith(SpringRunner.class)
public class HabitanteServiceImplIntegrationTest {

	@TestConfiguration
	static class HabitanteServiceImplTestContextConfiguration {
		
		@Bean
		public RestTemplate getRestTemplate() {
			return new RestTemplate();
		}

		@Bean
		public ViacepService viacepService() {
			return new ViacepService();
		}

		@Bean
		public HabitanteService habitanteService() {
			return new HabitanteService();
		}
		
		@Bean
		public EnderecoService enderecoService() {
			return new EnderecoService();
		}
		
		@MockBean
		private EnderecoRepository enderecoRepository;

	}

	@Autowired
	private HabitanteService habitanteService;


	@MockBean
	private HabitanteRepository habitanteRepository;

	
	@Before
	public void setUp() {
		Habitante alex = new Habitante("1234", "alex");
		Mockito.when(habitanteRepository.findByCodigo(alex.getCodigo())).thenReturn(alex);
	}

	
	@Test
	public void whenValidCodigo_thenHabitanteShouldBeFound() {
		String codigo = "1234";
		Habitante found = habitanteService.getHabitanteByCodigo(codigo);

		assertThat(found.getCodigo()).isEqualTo(codigo);
	}
}
