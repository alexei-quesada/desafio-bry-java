package teste.backend;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import teste.backend.entities.Habitante;
import teste.backend.repository.HabitanteRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HabitanteRepositoryIntegrationTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private HabitanteRepository employeeRepository;
 
    @Test
    public void whenFindByCodigo_thenReturnHabitante() {
        // given
        Habitante alex = new Habitante("123457","alex");
        entityManager.persist(alex);
        entityManager.flush();
     
        // when
        Habitante found = employeeRepository.findByCodigo(alex.getCodigo());
     
        // then
        assertThat(found.getCodigo()).isEqualTo(alex.getCodigo());
    }
 
}
