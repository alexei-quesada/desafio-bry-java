package teste.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import teste.backend.entities.Habitante;

/**
 * @author Alexei
 */

public interface HabitanteRepository extends JpaRepository<Habitante, Integer> {

	Habitante findByCodigo(String codigo);

}
