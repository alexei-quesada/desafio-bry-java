package teste.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import teste.backend.entities.Endereco;

/**
 * @author Alexei
 */

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {
	
	
	 @Query(value = "SELECT e FROM Endereco e WHERE e.habitante.id = :habitanteId and e.codigoPostal = :codigoPostal and e.numero = :numero")
	 Endereco findByHabitanteAndCepAndNumero(
			 @Param("habitanteId") long  habitanteId,
			 @Param("codigoPostal") String  codigoPostal,
			 @Param("numero") int numero);
	
}
