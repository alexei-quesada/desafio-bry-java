package teste.backend.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import teste.backend.DTOs.HabitanteDTO;
import teste.backend.entities.Habitante;
import teste.backend.events.ResourceCreated;
import teste.backend.services.HabitanteService;

/**
 * @author Alexei
 */

@CrossOrigin
@RestController
public class HabitanteController {

	@Autowired
	private HabitanteService habitanteService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	
	/** 
	 * GET /habitantes
	 * 
	 * Devolve todos os habitantes cadastrados junto com seus endereços
     */
	@GetMapping("/habitantes")
	public List<HabitanteDTO> getHabitantes() {

		// obter os habitantes cadastrados
		List<Habitante> listHabitantes = habitanteService.getHabitantes();
		
		// Converte a lista de objetos para lista DTO resultado
		return convertEntityListToDto(listHabitantes);
	}


	/**
	 * GET /habitantes/{codigo}
	 * 
	 * Devolve as informações de um habitante e seus endereços
	 * @PathVariable codigo
     */
	@GetMapping("/habitantes/{codigo}")
	public HabitanteDTO getHabitanteByCodigo(@PathVariable String codigo) {

		// Obtém o habitante dado o código
		Habitante hab = habitanteService.getHabitanteByCodigo(codigo);
		
		// Converte o objeto para DTO resultado
		return convertEntityToDto(hab);
	}
	
	
	/**
	 * POST /habitantes
	 * 
	 * Cadastrar um novo habitante
	 * @RequestBody HabitanteDTO
     */
	@PostMapping("/habitantes")
	@ResponseStatus(HttpStatus.CREATED)
	public HabitanteDTO addHabitante(@RequestBody HabitanteDTO habitanteDto, HttpServletResponse response) {	
	
		// adicionar o novo habitante
		Habitante habitante = habitanteService.addHabitante(habitanteDto);

		// publica o evento de criação de recurso e coloca a URL de acesso no header Location
		eventPublisher.publishEvent(new ResourceCreated(this, response, habitante.getCodigo()));
		
		// Converte o objeto para DTO resultado
		HabitanteDTO habDto = convertEntityToDto(habitante);
		return habDto;
	}
	

	/**
	 * PUT /habitantes/{codigo}
	 * 
	 * Atualiza um habitante com as informações entradas
	 * @PathVariable codigo
	 * @RequestBody HabitanteDTO
     */
	@PutMapping("/habitantes/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public String updateHabitante(@PathVariable String codigo, @RequestBody HabitanteDTO habitanteDto) {

		habitanteService.updateHabitante(codigo, habitanteDto);
		return "";
	}
	
	
	/**
	 * DELETE /habitantes/{codigo}
	 * 
	 * Remove um habitante dado o código
	 * @PathVariable codigo
     */
	@DeleteMapping("/habitantes/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public String deleteHabitanteByCodigo(@PathVariable String codigo) {

		habitanteService.deleteHabitanteByCodigo(codigo);
		return "";
	}

	
	/**
	 * Função utilitária para converter uma Entity em um objeto DTO
	 * @param Habitante
     */
	private HabitanteDTO convertEntityToDto(Habitante habitante) {
		HabitanteDTO habitanteDto = modelMapper.map(habitante, HabitanteDTO.class);
		return habitanteDto;
	}
	

	/**
	 * Função utilitária para converter uma lista de entidades em uma lista de objetos DTO
	 * @param List<Habitante>
     */
	private List<HabitanteDTO> convertEntityListToDto(List<Habitante> habitantes) {

		List<HabitanteDTO> listDto = new ArrayList<>();

		for (Habitante habitante : habitantes) {
			listDto.add(convertEntityToDto(habitante));
		}

		return listDto;
	}

}
