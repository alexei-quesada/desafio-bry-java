package teste.backend.exceptions;

/**
 * @author Alexei
 */

public class ViaCEPException extends Exception {
	
	private String CEP;
    private String Classe;
    
    
    public ViaCEPException(String message, String cep, String classe) {
        super(message);
        
        this.CEP = cep;
        this.Classe = classe;
    }


	public String getCEP() {
		return CEP;
	}


	public void setCEP(String cEP) {
		CEP = cEP;
	}


	public String getClasse() {
		return Classe;
	}


	public void setClasse(String classe) {
		Classe = classe;
	}

    

}
