package teste.backend.DTOs;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Alexei
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class HabitanteDTO {

	private String codigo;
	private String nome;
	private List<EnderecoDTO> enderecos = new ArrayList<>();

	public HabitanteDTO() {
	}

	public HabitanteDTO(String codigo, String nome, List<EnderecoDTO> enderecos) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.enderecos = enderecos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<EnderecoDTO> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoDTO> enderecos) {
		this.enderecos = enderecos;
	}

}
