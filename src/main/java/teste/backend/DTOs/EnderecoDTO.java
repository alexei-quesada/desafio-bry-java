package teste.backend.DTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Alexei
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoDTO {

	private String codigoPostal;
	private String logradouro;
	private String bairro;
	private int numero;
	private String complemento;
	private String localidade;
	private String uf;

	public EnderecoDTO() {
	}

	public EnderecoDTO(String codigoPostal, String logradouro, String bairro, int numero, String complemento,
			String localidade, String uf) {
		super();
		this.codigoPostal = codigoPostal;
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.numero = numero;
		this.complemento = complemento;
		this.localidade = localidade;
		this.uf = uf;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	

}
