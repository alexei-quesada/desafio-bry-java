package teste.backend.events;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

/**
 * @author Alexei
 */

public class ResourceCreated extends ApplicationEvent {
    private HttpServletResponse response;
    private String idOfNewResource;
 
    public ResourceCreated(Object source, 
      HttpServletResponse response, String idOfNewResource) {
        super(source);
 
        this.response = response;
        this.idOfNewResource = idOfNewResource;
    }
 
    public HttpServletResponse getResponse() {
        return response;
    }
    public String getIdOfNewResource() {
        return idOfNewResource;
    }
}
