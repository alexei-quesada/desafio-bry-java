package teste.backend.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import teste.backend.DTOs.HabitanteDTO;
import teste.backend.DTOs.ViaCepResponseDTO;
import teste.backend.entities.Endereco;
import teste.backend.entities.Habitante;
import teste.backend.exceptions.ViaCEPException;
import teste.backend.repository.HabitanteRepository;

/**
 * @author Alexei
 */

@Service
public class HabitanteService {

	@Autowired
	private HabitanteRepository habitanteRepository;

	@Autowired
	private ViacepService viacepService;

	@Autowired
	private EnderecoService enderecoService;

	
	/**
	 * Devolve todos os habitantes cadastrados junto com seus endereços
     */
	public List<Habitante> getHabitantes() {
		return habitanteRepository.findAll();
	}
	
	
	/**
	 * Devolve as informações de um habitante e seus endereços
	 * @param codigo
     */
	public Habitante getHabitanteByCodigo(String codigo) {
		
		// obtém o habitante dado o código
		Habitante hab = habitanteRepository.findByCodigo(codigo);

		// se o habitante não for encontrado, retorna o Status 404
		if (hab == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Habitante não encontrado");
		} else {
			return hab;
		}
	}
	

	/**
	 * Cadastrar um novo habitante
	 * @param HabitanteDTO
     */
	public Habitante addHabitante(HabitanteDTO habitanteDto) {

		// buscar o habitante no banco de dados a partir do código
		Habitante habitante = habitanteRepository.findByCodigo(habitanteDto.getCodigo());

		// se o habitante já existe, retorna o Status 409
		if (habitante != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Esse habitante já existe");
		}

		try {
			// cria um novo objeto Habitante
			habitante = new Habitante();
			
			// obter nome e códigos enviados no corpo da requisição
			habitante.setCodigo(habitanteDto.getCodigo());
			habitante.setNome(habitanteDto.getNome());

			// iterar todos os endereços do habitante
			for (int i = 0; i < habitanteDto.getEnderecos().size(); i++) {

				// consultar o serviço viacep passando como parâmetro o CEP
				ViaCepResponseDTO viaCepResponseDTO = viacepService.buscarEnderecoCEP(habitanteDto.getEnderecos().get(i).getCodigoPostal());

				// se o complemento é informado na requisição, é salvo, caso contrário, é obtido da resposta Viacep 
				String complemento = (habitanteDto.getEnderecos().get(i).getComplemento() != null) ?
						  habitanteDto.getEnderecos().get(i).getComplemento()
						: viaCepResponseDTO.getComplemento();

				// cria um novo objeto Endereco	  
				Endereco endereco = new Endereco();
						  
				// salva as informações obtidas da API Viacep		  		
				endereco.setCodigoPostal(habitanteDto.getEnderecos().get(i).getCodigoPostal());
				endereco.setLogradouro(viaCepResponseDTO.getLogradouro());
				endereco.setBairro(viaCepResponseDTO.getBairro());
				endereco.setNumero(habitanteDto.getEnderecos().get(i).getNumero());
				endereco.setComplemento(complemento);
				endereco.setLocalidade(viaCepResponseDTO.getLocalidade());
				endereco.setUf(viaCepResponseDTO.getUf());

				// adiciona o endereço ao habitante
				habitante.addEndereco(endereco);
			}

			// persiste o habitante no banco de dados
			return habitanteRepository.save(habitante);

		} catch (Exception ex) {
			// se uma exceção do tipo ViacepException for capturada, o mensagem CEP inválido será informado,
			// caso contrário, a exceção capturada será informada
			// Status 400
			if (ex instanceof ViaCEPException)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "CEP inválido", ex);
			else
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
		}
	}

	/**
	 * Atualiza um habitante com as informações entradas
	 * @param codigo
	 * @param HabitanteDTO
     */
	public void updateHabitante(String codigo, HabitanteDTO habitanteDto) {

		//verifique se o habitante existe
		Habitante habitante = habitanteRepository.findByCodigo(codigo);

		//se não existir, devolve o status 404
		if (habitante == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Esse habitante não existe");
		}

		try {
			
			// obter nome e códigos enviados no corpo da requisição
			habitante.setCodigo(habitanteDto.getCodigo());
			habitante.setNome(habitanteDto.getNome());

			// iterar todos os endereços do habitante
			for (int i = 0; i < habitanteDto.getEnderecos().size(); i++) {

				// obter cep, numero e complemento se houver
				String cep = habitanteDto.getEnderecos().get(i).getCodigoPostal();
				int numero = habitanteDto.getEnderecos().get(i).getNumero();
				String complemento = habitanteDto.getEnderecos().get(i).getComplemento();

				// verificar que o endereço não existe em BD,
				// um endereço é considerado o mesmo se tiver o mesmo CEP e número		
				Endereco endereco = enderecoService.getEnderecoByHabitanteCepNumero(habitante.getId(), cep, numero);

				if (endereco == null) {
									
					// consultar o serviço viacep passando como parâmetro o CEP
					ViaCepResponseDTO viaCepResponseDTO = viacepService.buscarEnderecoCEP(cep);
					
					// cria um novo objeto Endereco
					endereco = new Endereco();
					
					// salva as informações obtidas da API Viacep	
					endereco.setCodigoPostal(cep);
					endereco.setLogradouro(viaCepResponseDTO.getLogradouro());
					endereco.setBairro(viaCepResponseDTO.getBairro());
					endereco.setNumero(numero);
					endereco.setComplemento(viaCepResponseDTO.getComplemento());
					endereco.setLocalidade(viaCepResponseDTO.getLocalidade());
					endereco.setUf(viaCepResponseDTO.getUf());

					// adiciona o novo endereço ao habitante
					habitante.addEndereco(endereco);
				}
				
				//se o complemento é informado no corpo da requisição, se atualiza
				if(complemento != null)
					endereco.setComplemento(complemento);
			}

			// atualiza o habitante no banco de dados
			habitanteRepository.save(habitante);

		} catch (Exception ex) {
			// se uma exceção do tipo ViacepException for capturada, o mensagem CEP inválido será informado,
			// caso contrário, a exceção capturada será informada
			// Status 400
			if (ex instanceof ViaCEPException)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "CEP inválido", ex);
			else
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
		}
	}

	/**
	 * Remove um habitante dado o código
	 * @param codigo
     */
	public void deleteHabitanteByCodigo(String codigo) {
		
		//verifique se o habitante existe
		Habitante habitante = habitanteRepository.findByCodigo(codigo);

		// se o habitante não for encontrado, retorna status 404
		if (habitante == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Esse habitante não existe");
		} else {
			
			// remove o habitante do banco de dados
			habitanteRepository.delete(habitante);
		}
	}

}
