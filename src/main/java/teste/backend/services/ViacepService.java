package teste.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import teste.backend.DTOs.ViaCepResponseDTO;
import teste.backend.exceptions.ViaCEPException;

/**
 * @author Alexei
 */

@Service
public class ViacepService {

	@Autowired
	private RestTemplate restTemplate;
	
	
	/** 
	 * Consulta API Viacep
	 * @param cep
     */
	public ViaCepResponseDTO buscarEnderecoCEP(String cep) throws ViaCEPException, RestClientException {
		
		String url = "";

		//verifica se o formato é válido CEP
		if(validaCep(cep)) {
			
			 // criar o URL da API
			 url = "http://viacep.com.br/ws/" + cep + "/json/";
			 
			 // faz a requisição à API Viacep
			 ViaCepResponseDTO viacepResponse = restTemplate.getForObject(url, ViaCepResponseDTO.class);
			 
			 // se o CEP informado não tiver um valor válido lança exceção
			 if(viacepResponse.isErro()) {
				 throw new ViaCEPException("O CEP informado tem um formato inválido", cep, ViaCEPException.class.getName());
			 }
			 
			 
			 else {
				 return viacepResponse;
			 }
		 
		}		
		
		// se o formato inválido lançar exceção
		else {
			throw new ViaCEPException("O CEP informado tem um formato inválido", cep, ViaCEPException.class.getName());
		}
	}
	
	// validar formato del CEP
	private boolean validaCep(String cep) {
		if (!cep.matches("\\d{8}")) {
			return false;
		}
		return true;
	}

}