package teste.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teste.backend.entities.Endereco;
import teste.backend.repository.EnderecoRepository;

/**
 * @author Alexei
 */

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	
	/**
	 * encontrar um endereço dado os seguintes parâmetros:
	 * @param habitanteId
	 * @param cep
	 * @param numero
     */
	public Endereco getEnderecoByHabitanteCepNumero(long habitanteId, String cep, int numero) {
		return enderecoRepository.findByHabitanteAndCepAndNumero(habitanteId, cep, numero);
	}

	/**
	 * Persiste/atualiza endereço no banco de dados
	 * @param Endereco
     */
	public Endereco saveEndereco(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}

}
