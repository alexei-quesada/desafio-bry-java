<h1>Desafio BRy Tecnologia - Desenvolvedor Java Pleno</h1>


<h2>Requisitos</h2>

Para executar o aplicativo, você precisa:

    
*  Java 8 (ou superior)

*  Maven 3 (opcional)

<h2>Executando o aplicativo localmente</h2>

Usando o Maven:


> mvn spring-boot:run 

Executando o JAR:


> java - jar jar/teste-app.jar 


O aplicativo será executado localmente no porto 8082

http://localhost:8082/